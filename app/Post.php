<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title','content','post_category_id','featured','slug'
    ];
    public function getFeaturedLink($featured){
       return asset($featured);
    }
    protected $dates = [
        'deleted_at',
    ];
    public function post_categories(){
        return $this->belongsTo('App\PostCategory');
    }
    public function tags(){
        return $this->belongsToMany('App\Tag');
    }
}
