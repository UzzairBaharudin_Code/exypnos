<?php

namespace App\Http\Controllers;

use Session;
use App\Post;
use App\PostCategory;
use App\Tag;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('backend.posts.index')->with('posts',Post::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post_categories = PostCategory::all();
        $tags = Tag::all();

        if($post_categories->count() ==0 || $tags->count() == 0){

            Session::flash('info','You have to add a category first before adding a post');
            return redirect()->back();
        }

        return view('backend.posts.create')->with('post_categories',PostCategory::all())
                                           ->with('tags',Tag::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'featured'=>'required|image',
            'content'=>'required',
            'post_category_id'=>'required',
            'tags'=>'required'
        ]);
        
        $featured = $request->featured;
        $featured_new_name = time().$featured->getClientOriginalName();
        $featured->move('uploads/posts',$featured_new_name);

        $post = Post::create([
            'title'=>$request->title,
            'slug' =>str_slug($request->title),
            'content'=>$request->content,
            'featured' => '/uploads/posts/'.$featured_new_name,
            'post_category_id'=> $request->post_category_id            
        ]);
        //Attach to pivot table: post_tag since Post - Tag is a many to many relationship
        $post->tags()->attach($request->tags);
        Session::flash('success','The post was saved successfully');
        return redirect()->route('posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);       

        return view('backend.posts.edit')
        ->with('post',$post)
        ->with('post_categories', PostCategory::all())
        ->with('tags', Tag::all());        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'post_category_id' => 'required',
            'tags' => 'required'
        ]);

        $post = Post::find($id);

        if($request->hasFile('featured')){
            $this->validate($request,[
                'featured'=>'required|image'
            ]);
            $featured=$request->featured;
            $featured_new_name = time().$featured->getClientOriginalName();
            $featured->move('uploads/posts',$featured_new_name);
            $post->featured = '/uploads/posts/'.$featured_new_name;
        }
        $post->title = $request->title;
        $post->content = $request->content;
        $post->post_category_id = $request->post_category_id;
       
        $post->save();
        $post->tags()->sync($request->tags);
        Session::flash('success','The post was successfully update');
        return redirect()->route('posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();
        Session::flash('deleted','The post was successfully deleted');
        return redirect()->route('posts');
    }

    public function trashed(){
        $posts = Post::onlyTrashed()->get();
        return view('backend.posts.trashed')->with('posts',$posts);
    }

    public function kill($id){
        $post = Post::withTrashed()->where('id', $id)->first();
        $post->forceDelete();
        Session::flash('success','The post was permanently deleted');
        return redirect()->back();
    }

    public function restore($id){
        $post = Post::withTrashed()->where('id', $id)->first();
        $post->restore();
        Session::flash('success','The post was successfully resotred');
        return redirect()->route('posts');
    }
}