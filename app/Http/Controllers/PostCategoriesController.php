<?php

namespace App\Http\Controllers;

use Session;
use App\PostCategory;
use Illuminate\Http\Request;

class PostCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post_categories = PostCategory::all();
        return view('backend.post_categories.index')->with('post_categories', $post_categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.post_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:255'
        ]);

        $category = new PostCategory;
        $category->name = $request->name;
        $category->save();
        Session::flash('success','The category: '.$category->name.' was successfully created');
        return redirect()->route('post_categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post_category = PostCategory::find($id);
        return view('backend.post_categories.edit')->with('post_category',$post_category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post_category = PostCategory::find($id);
        $this->validate($request,[
            'name'=>'required|min:5'
        ]);
        $post_category->name = $request->name;
        $post_category->save();
        return redirect()->route('post_categories');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $category = PostCategory::find($id);

        foreach($category->posts as $post){
            $post->delete();
        }
        $category->delete();
        Session::flash('success','The category: '.$category->name.' was successfully deleted');
        return redirect()->route('post_categories');
    }
}
