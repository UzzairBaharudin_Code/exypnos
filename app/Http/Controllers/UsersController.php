<?php

namespace App\Http\Controllers;
use Session;
use App\User;
use App\Profile;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    //don't let non admin perform operation under Users method
    public function __construct(){
        $this->middleware('admin');
    }
    public function index(){
        return view('backend.users.index')->with('users', User::all());
    }

    public function create(){
        return view('backend.users.create');
    }

    public function store(Request $request){

        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            // 'password' => 'required'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' =>$request->email,
            'password' => bcrypt('password')
        ]);

        $profile = Profile::create([
            'user_id' => $user->id,  
            'avatar' => 'uploads/avatars/1.png'
        ]);

        Session::flash('success','User was addes successfully' );
        return redirect()->route('users');
    }

    public function admin($id){
        
        $user = User::find($id);
        $user->admin = 1;
        $user->save();
        Session::flash('success', 'The user '.$user->name.' has become admin' );
        return redirect()->route('users');
        
    }
    public function non_admin($id){
        
        $user = User::find($id);
        $user->admin = 0;
        $user->save();
        Session::flash('success', 'The user '.$user->name.' has become admin' );
        return redirect()->route('users');
        
    }

    public function edit($id){
        $user = User::find($id);
        return view('backend.users.edit')->with('user', $user);
    }

    public function delete($id){
        $user = User::find($id);
        $user->profile->delete();
        $user->delete();
        Session::flash('success','The user was successfully deleted');
        return redirect()->route('users');
    }
}
