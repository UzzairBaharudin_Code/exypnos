<?php

namespace App\Http\Middleware;

use Auth;
use Session;
use Closure;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    // if the user is not admin
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->admin){
           Session::flash('info', 'Sorry, you do not have the permission to perform this operation');
           return redirect()->back();
        }

        
        return $next($request);
    }
}
