<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Setting::create([

        'site_name' => 'Exypnos CMS',
        'address' => 'Ipoh',
        'contact_number' => '012910102',
        'contact_email' => 'hello@gmail.com'
        ]);
    }
}
