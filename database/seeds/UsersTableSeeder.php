<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'Uzzair Baharudin',
            'email' => 'uzzairwork@gmail.com',
            'password' => bcrypt('sengal91'),
            'admin' => 1
        ]);
        
        App\Profile::create([
            'user_id' => $user->id,
            'avatar'=>'uploads/avatars/1.png',
            'about' => 'Lorem ipsum dolor sit amet',
            'facebook'=>'http://facebook.com',
            'youtube' =>'http://youtube.com'
        ]);
    }
}
