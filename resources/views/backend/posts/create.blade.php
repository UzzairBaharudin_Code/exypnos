@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Create New Post
			</h6>
			<div class="element-box">
				<form action="{{route('post.store')}}" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for=""> Title</label>
						<input name="title" type="text" class="form-control"> 
            @if ($errors->has('title'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('title') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('featured') ? ' has-error' : '' }}">
						<label for=""> Featured</label>
						<input name="featured" type="file" class="form-control">
            @if ($errors->has('featured'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('featured') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group">
						<label for=""> Post Category</label>
						<select name="post_category_id" id="" class="form-control">
							@foreach($post_categories as $category)
							<option value="{{$category->id}}">{{$category->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for=""> Post Tag</label>
					
							@foreach($tags as $tag)
							<div class="checkbox">
							<!-- use tags[] since its an array -->
							<label> <input type="checkbox" name="tags[]" id="" value="{{$tag->id}}"> {{$tag->tag}}</label>
							</div>							
							@endforeach					
					</div>
					<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
						<label for="">
							Content
						</label>
						<textarea class="form-control" name="content" id="content" cols="30" rows="10"></textarea>						
            @if ($errors->has('content'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('content') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-buttons-w">
						<button class="btn btn-primary" type="submit"> Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop


@section('ckeditor-js')
 <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
<script>
ClassicEditor
    .create( document.querySelector( '#content' ) )
    .then( editor => {
        console.log( editor );
    } )
    .catch( error => {
        console.error( error );
    } );
</script>
@stop