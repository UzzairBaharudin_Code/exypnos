@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Posts
			</h6>
			<div class="element-box">

				<div class="table-responsive">
					<table id="datatable" class="table table-striped ">
						<thead>
							<tr>
								<th>#</th>
								<th>Image</th>
								<th>Post Name</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($posts as $key=>$post)
							<tr>
								<td>{{++$key}}</td>
								<td>
									<img src="{{$post->featured}}" class="img-responsive" width="100px" />
								</td>
								<td>{{$post->title}}</td>
								<td class="row-actions">
									<a href="{{route('post.edit',['id'=>$post->id])}}">
										<i class="os-icon os-icon-pencil-2"></i>
									</a>
									<a class="danger" href="{{route('post.delete',['id'=>$post->id])}}">
										<i class="os-icon os-icon-ui-15"></i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection