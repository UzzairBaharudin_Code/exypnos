@extends('backend.master')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Edit Post: {{$post->title}}
			</h6>
			<div class="element-box">
				<form action="{{route('post.update',['id'=>$post->id])}}" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
						<label for=""> Title</label>
						<input name="title" type="text" class="form-control" value="{{$post->title}}"> 
            @if ($errors->has('title'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('title') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('featured') ? ' has-error' : '' }}">
						<label for=""> Featured</label>
                        <img src="{{$post->featured}}" width="300px" alt="">
						<input name="featured" type="file" class="form-control" value="{{$post->featured}}">
            @if ($errors->has('featured'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('featured') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group">
						<label for=""> Category</label>
						<select name="post_category_id" id="" class="form-control">
							@foreach($post_categories as $category)
							<option value="{{$category->id}}" 
							@if($post->post_category_id == $category->id)
								selected
							@endif>
							{{ $category->name }}</option>							
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for=""> Post Tag</label>
					
							@foreach($tags as $tag)
							<div class="checkbox"><!-- use tags[] since its an array -->
							<label> 
							<input type="checkbox" name="tags[]" id="" value="{{$tag->id}}"
						  @foreach($post->tags as $t)
								@if($tag->id == $t->id) 
							checked 
							@endif
							@endforeach> 
							{{$tag->tag}}
							</label>
							</div>							
							@endforeach					
					</div>
					<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">			
						<label for="">
							Content
						</label>
						<textarea name="content" class="form-control" id="content" cols="30" rows="10">{{$post->content}}</textarea>
            @if ($errors->has('content'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('content') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-buttons-w">
						<button class="btn btn-primary" type="submit"> Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@section('ckeditor-js')
 <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
<script>
ClassicEditor
    .create( document.querySelector( '#content' ) )
    .then( editor => {
        console.log( editor );
    } )
    .catch( error => {
        console.error( error );
    } );
</script>
@stop