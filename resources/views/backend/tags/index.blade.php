@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Tags
			</h6>
			<div class="element-box">

				<div class="table-responsive">
					<table id="datatable" class="table table-striped ">
						<thead>
							<tr>
								<th>#</th>
								<th>Tag</th>								
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($tags as $key=>$tag)
							<tr>
								<td>{{++$key}}</td>								
								<td>{{$tag->tag}}</td>
								<td class="row-actions">
									<a href="{{route('tag.edit',['id'=>$tag->id])}}">
										<i class="os-icon os-icon-pencil-2"></i>
									</a>
									<a class="danger" href="{{route('tag.destroy',['id'=>$tag->id])}}">
										<i class="os-icon os-icon-ui-15"></i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection