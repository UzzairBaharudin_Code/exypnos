@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Create New Tag
			</h6>
			<div class="element-box">
				<form action="{{route('tag.store')}}" method="post">
					{{csrf_field()}}
					<div class="form-group{{ $errors->has('tag') ? ' has-error' : '' }}">
						<label for="">Tag</label>
						<input name="tag" type="text" class="form-control"> 
            @if ($errors->has('tag'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('tag') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-buttons-w">
						<button class="btn btn-primary" type="submit"> Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop