<div class="top-menu-secondary with-overflow color-scheme-dark">
	<!--------------------
        START - Messages Link in secondary top menu
        -------------------->
	<div class="logo-w menu-size">
		<a class="logo" href="index.html">
			<img src="{{asset('img/logo.png')}}">
			<span>Exypnos</span>
		</a>
	</div>
	<div class="top-menu-controls">
		<div class="element-search hidden-lg-down">
			<input placeholder="Start typing to search..." type="text">
		</div>
		<div class="top-icon top-search hidden-xl-up">
			<i class="os-icon os-icon-ui-37"></i>
		</div>
		<!--------------------
          START - Messages Link in secondary top menu
          -------------------->
		<div class="messages-notifications os-dropdown-trigger os-dropdown-center">
			<i class="os-icon os-icon-mail-14"></i>
			<div class="new-messages-count">
				12
			</div>
			<div class="os-dropdown light message-list">
				<div class="icon-w">
					<i class="os-icon os-icon-mail-14"></i>
				</div>
				<ul>
					<li>
						<a href="#">
							<div class="user-avatar-w">
								<img alt="" src="{{asset('img/avatar1.jpg')}}">
							</div>
							<div class="message-content">
								<h6 class="message-from">
									John Mayers
								</h6>
								<h6 class="message-title">
									Account Update
								</h6>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div class="user-avatar-w">
								<img alt="" src="{{asset('img/avatar2.jpg')}}">
							</div>
							<div class="message-content">
								<h6 class="message-from">
									Phil Jones
								</h6>
								<h6 class="message-title">
									Secutiry Updates
								</h6>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div class="user-avatar-w">
								<img alt="" src="{{asset('img/avatar5.jpg')}}">
							</div>
							<div class="message-content">
								<h6 class="message-from">
									Bekky Simpson
								</h6>
								<h6 class="message-title">
									Vacation Rentals
								</h6>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div class="user-avatar-w">
								<img alt="" src="{{asset('img/avatar4.jpg')}}">
							</div>
							<div class="message-content">
								<h6 class="message-from">
									Alice Priskon
								</h6>
								<h6 class="message-title">
									Payment Confirmation
								</h6>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!--------------------
          END - Messages Link in secondary top menu
          -------------------->
		<!--------------------
          START - Settings Link in secondary top menu
          -------------------->
		<!--------------------
          END - Settings Link in secondary top menu
          -------------------->
		<!--------------------
          START - User avatar and menu in secondary top menu
          -------------------->
		<div class="logged-user-w">
			<div class="logged-user-i">
				<div class="avatar-w">
					<img alt="" src="{{asset(Auth::user()->profile->avatar)}}">
				</div>
				<div class="logged-user-menu">
					<div class="logged-user-avatar-info">
						<div class="avatar-w">
							<img alt="" src="{{asset(Auth::user()->profile->avatar)}}">
						</div>
						<div class="logged-user-info-w">
							<div class="logged-user-name">
								{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
							</div>
							<div class="logged-user-role">
								Administrator
							</div>
						</div>
					</div>
					<div class="bg-icon">
						<i class="os-icon os-icon-wallet-loaded"></i>
					</div>
					<ul>						
						<li>
							<a href="{{route('user.profile')}}">
								<i class="os-icon os-icon-user-male-circle2"></i>
								<span>Profile Details</span>
							</a>
						</li>
						<li>
							<a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
								<i class="os-icon os-icon-signs-11"></i>
								<span>Logout</span>
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!--
          END - User avatar and menu in secondary top menu
       -->
	</div>
</div>