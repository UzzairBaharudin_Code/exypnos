@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Post Categories
			</h6>
			<div class="element-box">

				<div class="table-responsive">
					<table id="datatable" class="table table-striped table-lightfont">
						<thead>
							<tr>
								<th>#</th>
								<th>Category Name</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($post_categories as $key=>$category)
							<tr>
								<td>{{++$key}}</td>
								<td>{{$category->name}}</td>
								<td class="row-actions">
									<a href="{{route('post_category.edit',['id'=>$category->id])}}">
										<i class="os-icon os-icon-pencil-2"></i>
									</a>
									<a class="danger" href="{{route('post_category.delete',['id'=>$category->id])}}">
										<i class="os-icon os-icon-ui-15"></i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection