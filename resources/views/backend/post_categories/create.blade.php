@extends('backend.master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Create A New Category</div>
    <div class="panel-body">
        <form action="{{route('post_category.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
            <label for="title">
                Category Name
            </label>
            <input name="name" type="text" class="form-control">
            </div>
            <div class="form-group">
            <button class="btn btn-primary" type="submit">
              Save
            </button>
            </div>
        </form>
    </div>
</div>
@stop