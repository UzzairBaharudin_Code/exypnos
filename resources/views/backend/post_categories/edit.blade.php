@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Edit Category: {{$post_category->name}}
			</h6>
			<div class="element-box">
				<form class="" action="{{route('post_category.update',['id'=>$post_category->id])}}" method="post">
					{{csrf_field()}}
					<div class="form-group">
						<input name="name" type="text" class="form-control" value="{{$post_category->name}}">
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Save Changes">
                        <a href="{{route('post_categories')}}" class="btn btn-info">Cancel</a>  
					</div> 
				</form>
			</div>
		</div>
	</div>
</div>


@stop