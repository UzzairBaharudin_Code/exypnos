<!DOCTYPE html>
<html>

<head>
	<title>{{ config('app.name', 'Exypnos') }}</title>
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<meta content="Uzzair Baharudin" name="author">
	<meta content="Exypnos CMS" name="description">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<link href="favicon.png" rel="shortcut icon">
	<link href="apple-touch-icon.png" rel="apple-touch-icon">	
	<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet">
	<link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">
	<link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/fullcalendar.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/main.css') }}" rel="stylesheet">
	 <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/perfect-scrollbar.min.css') }}" rel="stylesheet">
	 @yield('ckeditor-css')
</head>
<body>
	<div class="all-wrapper menu-side solid-bg-all">
		@include('backend.includes.top')
		<div class="layout-w">
			@include('backend.includes.sidebar')
			<div class="content-w">
				<!--START - Breadcrumbs-->
				<ul class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="index.html">Home</a>
					</li>
					<li class="breadcrumb-item">
						<a href="index.html">Products</a>
					</li>
					<li class="breadcrumb-item">
						<span>Laptop with retina screen</span>
					</li>
				</ul>
				<!--END - Breadcrumbs-->
				<div class="all-wrapper menu-side with-side-panel">
					<div class="layout-w">
						<div class="content-w">
							<div class="content-panel-toggler">
								<i class="os-icon os-icon-grid-squares-22"></i>
								<span>Sidebar</span>
							</div>
							<div class="content-i">
							
              {{--  START - Content  --}}
								<div class="content-box">									
									@yield('content')
								</div>
              {{--  END - Content  --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="display-type"></div>
	</div>
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	@yield('ckeditor-js')
	<script src="{{ asset('js/moment.js') }}"></script>
	<script src="{{ asset('js/select2.full.min.js') }}"></script>
	<script src="{{ asset('js/Chart.min.js') }}"></script>
	<script src="{{ asset('js/validator.min.js') }}"></script>
	<script src="{{ asset('js/daterangepicker.js') }}"></script>
	<script src="{{ asset('js/dropzone.js') }}"></script>
	<script src="{{ asset('js/mindmup-editabletable.js') }}"></script>
	<script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
	<script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
	<script src="{{ asset('js/tether.min.js') }}"></script>
	<script src="{{ asset('js/util.js') }}"></script>
	<script src="{{ asset('js/alert.js') }}"></script>
	<script src="{{ asset('js/button.js') }}"></script>
	<script src="{{ asset('js/carousel.js') }}"></script>
	<script src="{{ asset('js/collapse.js') }}"></script>
	<script src="{{ asset('js/dropdown.js') }}"></script>
	<script src="{{ asset('js/modal.js') }}"></script>
	<script src="{{ asset('js/tab.js') }}"></script>
	<script src="{{ asset('js/tooltip.js') }}"></script>
	<script src="{{ asset('js/popover.js') }}"></script>	
	<script src="{{ asset('js/toastr.min.js') }}"></script>
	<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
	<script type="text/javascript">
		@if(Session::has('status'))
			toastr.success("{{Session::get('status')}}");
		@endif
        @if(Session::has('success'))
            toastr.success("{{Session::get('success')}}");
        @endif
        @if(Session::has('info'))
            toastr.info("{{Session::get('info')}}");
        @endif
        @if(Session::has('deleted'))
            toastr.success("{{Session::get('deleted')}}");
        @endif    
    </script>
	<script src="{{ asset('js/main.js?version=3.6.1') }}"></script>
     
</body>
</html>