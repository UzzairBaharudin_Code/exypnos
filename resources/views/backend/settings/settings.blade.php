@extends('backend.master')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Edit Blog Settings
			</h6>
			<div class="element-box">
				<form action="{{route('settings.update')}}" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group{{ $errors->has('site_name') ? ' has-error' : '' }}">
						<label for=""> Site Name</label>
						<input name="site_name" type="text" class="form-control" value="{{$settings->site_name}}"> 
            @if ($errors->has('site_name'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('site_name') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
						<label for=""> Address</label>
						<input name="address" type="text" class="form-control" value="{{$settings->address}}"> 
            @if ($errors->has('address'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('address') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('contact_number') ? ' has-error' : '' }}">
						<label for=""> Phone Number</label>
						<input name="contact_number" type="text" class="form-control" value="{{$settings->contact_number}}"> 
            @if ($errors->has('contact_number'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('contact_number') }}</strong>
						</span>
						@endif
					</div>	
					<div class="form-group{{ $errors->has('contact_email') ? ' has-error' : '' }}">
						<label for=""> Email</label>
					<input name="contact_email" type="text" class="form-control" value="{{$settings->contact_email}}"> 
            @if ($errors->has('contact_email'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('contact_email') }}</strong>
						</span>
						@endif
					</div>				
					<div class="form-buttons-w">
						<button class="btn btn-primary" type="submit"> Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@section('ckeditor-js')
 <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
<script>
ClassicEditor
    .create( document.querySelector( '#content' ) )
    .then( editor => {
        console.log( editor );
    } )
    .catch( error => {
        console.error( error );
    } );
</script>
@stop