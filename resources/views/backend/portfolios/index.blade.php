@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Portfolios
			</h6>
			<div class="element-box">

				<div class="table-responsive">
					<table id="datatable" class="table table-striped ">
						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
								<th>Content</th>								
							</tr>
						</thead>
						<tbody>
							@foreach($portfolios as $key=>$portfolio)
							<tr>
								<td>{{++$key}}</td>
								<td>
									{{$portfolio->title}}
								</td>
								<td>{{$portfolio->content}}</td>								
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection