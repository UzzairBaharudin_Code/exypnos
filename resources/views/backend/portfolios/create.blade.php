@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Create New Portolio
			</h6>
			<div class="element-box">
				<form action="{{route('portfolios.store')}}" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for=""> Title</label>
						<input name="title" type="text" class="form-control"> 
            @if ($errors->has('title'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('title') }}</strong>
						</span>
						@endif
					</div>				
					<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
						<label for="">
							Content
						</label>
						<textarea class="form-control" name="content" id="content" cols="30" rows="10"></textarea>						
            @if ($errors->has('content'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('content') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-buttons-w">
						<button class="btn btn-primary" type="submit"> Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop


@section('ckeditor-js')
 <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
<script>
ClassicEditor
    .create( document.querySelector( '#content' ) )
    .then( editor => {
        console.log( editor );
    } )
    .catch( error => {
        console.error( error );
    } );
</script>
@stop