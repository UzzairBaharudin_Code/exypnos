@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Users
			</h6>
			<div class="element-box">

				<div class="table-responsive">
					<table id="datatable" class="table table-striped ">
						<thead>
							<tr>
								<th>#</th>
								<th>Image</th>
								<th>Name</th>
                                <th>Permission</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $key=>$user)
							<tr>
								<td>{{++$key}}</td>
								<td>
									<img src="{{asset($user->profile->avatar)}}" class="img-responsive" width="100px" />
								</td>
								<td>{{$user->name}}</td>
                                <td>
                                @if($user->admin)
                                    Admin
                                    <a href="{{route('user.non_admin',['id'=>$user->id])}}" class="btn btn-xs btn-info">Make As Non Admin</a>
                                @else
                                    Non-Admin
                                    <a href="{{route('user.admin',['id'=>$user->id])}}" class="btn btn-xs btn-info">Make As Admin</a>
                                @endif
                                </td>
								<td class="row-actions">
									<a href="{{route('user.edit',['id'=>$user->id])}}">
										<i class="os-icon os-icon-pencil-2"></i>
									</a>
									@if(Auth::id() != $user->id)
									<a class="danger" href="{{route('user.delete',['id'=>$user->id])}}">
										<i class="os-icon os-icon-ui-15"></i>
									</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection