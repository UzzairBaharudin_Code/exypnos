@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Edit User
			</h6>
			<div class="element-box">
				<form action="{{route('user.store')}}" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<label for=""> Name</label>
						<input name="name" type="text" class="form-control"> 
            @if ($errors->has('name'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
					</div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for=""> Email</label>
						<input name="email" type="text" class="form-control"> 
            @if ($errors->has('email'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif
					</div>
					{{--  <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
						<label for=""> Avatar</label>
						<input name="avatar" type="file" class="form-control">
            @if ($errors->has('avatar'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('avatar') }}</strong>
						</span>
						@endif
					</div>  --}}
					
					{{--  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="">
							Password
						</label>
						<input type="password" class="form-control">
            @if ($errors->has('content'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('content') }}</strong>
						</span>
						@endif
					</div>  --}}
					<div class="form-buttons-w">
						<button class="btn btn-primary" type="submit"> Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop