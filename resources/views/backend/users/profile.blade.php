@extends('backend.master') 
@section('content')

<div class="row">

	{{-- Profile Settings --}}
	<div class="col-sm-7">
		<div class="element-wrapper">
			<div class="element-box">
				<form method="post" enctype="multipart/form-data" action="{{route('user.profile.update')}}">
				{{csrf_field()}}
					<div class="element-info">
						<div class="element-info-with-icon">
							<div class="element-info-icon">
								<div class="os-icon os-icon-user-male-circle2"></div>
							</div>
							<div class="element-info-text">
								<h5 class="element-inner-header">
									Profile Settings
								</h5>
							</div>
						</div>
					</div>
					<div class="form-group">
							<div class="form-group">
								<label for="">Avatar</label>
								<input name="avatar" class="form-control" type="file">
								<img src="{{$user->profile->avatar}}" alt="">
								<div class="help-block form-text with-errors form-control-feedback"></div>
							</div>
					</div>
					<div class="form-group">
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="">Name</label>
								<input name="name" class="form-control" type="text" value="{{$user->name}}">
								@if ($errors->has('name'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
							</div>
					</div>
					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">						
						<label for=""> Email address</label>
						<input name="email" class="form-control" type="email" value="{{$user->email}}">
						@if ($errors->has('email'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label for=""> Password</label>
								<input name="password" class="form-control" type="password">
								<div class="help-block form-text text-muted form-control-feedback">
									Minimum of 6 character
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">								
							</div>
						</div>
					</div>
					<div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">						
						<label for=""> Facebook</label>
						<input name="facebook" class="form-control" type="text" value="{{$user->profile->facebook}}">
						@if ($errors->has('facebook'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('facebook') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('youtube') ? ' has-error' : '' }}">						
						<label for=""> Youtube</label>
						<input name="youtube" class="form-control" type="text" value="{{$user->profile->youtube}}">
						@if ($errors->has('youtube'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('youtube') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('about') ? ' has-error' : '' }}">						
						<label for="">About</label>
						<textarea name="about" class="form-control">{{$user->profile->about}}</textarea>
						@if ($errors->has('about'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('about') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-buttons-w">
						<button class="btn btn-primary" type="submit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	{{-- User Activity --}}
	<div class="col-sm-5">
		<div class="element-wrapper">
			<div class="element-box">
				<div class="timed-activities compact">
					<div class="element-info">
						<div class="element-info-with-icon">
							<div class="element-info-icon">
								<div class="os-icon os-icon-user-male-circle2"></div>
							</div>
							<div class="element-info-text">
								<h5 class="element-inner-header">
									Your Activity
								</h5>
							</div>
						</div>
					</div>
					<div class="timed-activity">
						<div class="ta-date">
							<span>21st Jan, 2017</span>
						</div>
						<div class="ta-record-w">
							<div class="ta-record">
								<div class="ta-timestamp">
									<strong>11:55</strong> am
								</div>
								<div class="ta-activity">
									Created a post called
									<a href="#">Register new symbol</a> in Rogue
								</div>
							</div>
							<div class="ta-record">
								<div class="ta-timestamp">
									<strong>2:34</strong> pm
								</div>
								<div class="ta-activity">
									Commented on story
									<a href="#">How to be a leader</a> in
									<a href="#">Financial</a> category
								</div>
							</div>
							<div class="ta-record">
								<div class="ta-timestamp">
									<strong>7:12</strong> pm
								</div>
								<div class="ta-activity">
									Added
									<a href="#">John Silver</a> as a friend
								</div>
							</div>
						</div>
					</div>
					<div class="timed-activity">
						<div class="ta-date">
							<span>3rd Feb, 2017</span>
						</div>
						<div class="ta-record-w">
							<div class="ta-record">
								<div class="ta-timestamp">
									<strong>9:32</strong> pm
								</div>
								<div class="ta-activity">
									Added
									<a href="#">John Silver</a> as a friend
								</div>
							</div>
							<div class="ta-record">
								<div class="ta-timestamp">
									<strong>5:14</strong> pm
								</div>
								<div class="ta-activity">
									Commented on story
									<a href="#">How to be a leader</a> in
									<a href="#">Financial</a> category
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection