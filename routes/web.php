<?php

// a testing route to find post-category-tag relationships
Route::get('/test',function(){
        //find PostCategory of id = 1 and look into its associated Post
        // return App\PostCategory::find(1)->posts;
        //find Tag of id = 1 and look into its associated Post
        // App\Tag::find(1)->posts;
        return App\Profile::find(1)->user;

});


Route::get('/',[

        'uses'=>'FrontEndController@index',
        'as' => 'index'
]); 

Auth::routes(); 

//Admin 
Route::group(['prefix' => 'vathos', 'middleware' => 'auth'], function () {

Route::get('/', 'HomeController@index') -> name('home'); 

Route::get('/posts', [
    'uses' => 'PostsController@index',
'as' => 'posts'
]); 
Route::get('/posts/trashed', [
        'uses' => 'PostsController@trashed', 
'as' => 'posts.trashed'
]); 

Route::get('/post/edit/{id}', [
        
            'uses' => 'PostsController@edit', 
'as' => 'post.edit'
]); 
Route::post('/post/update/{id}', [
                    'uses' => 'PostsController@update', 
'as' => 'post.update'
]); 

Route::get('/post/create', [

    'uses' => 'PostsController@create', 
'as' => 'post.create'
]); 

Route::post('/post/store', [

    'uses' => 'PostsController@store', 
'as' => 'post.store'
]); 

Route::get('/post/delete/{id}', [
        
            'uses' => 'PostsController@delete', 
'as' => 'post.delete'
]); 

Route::get('/post/kill/{id}', [
        'uses' => 'PostsController@kill', 
'as' => 'post.kill'
]); 

Route::get('/post/restore/{id}', [
        'uses' => 'PostsController@restore', 
'as' => 'post.restore'
]); 

// Post Category

Route::get('/post_categories', [
        'uses' => 'PostCategoriesController@index', 
'as' => 'post_categories'
]); 

Route::get('/post_category/create', [

    'uses' => 'PostCategoriesController@create', 
'as' => 'post_category.create'
]); 

Route::post('/post_category/store', [

    'uses' => 'PostCategoriesController@store', 
'as' => 'post_category.store'
]); 

Route::get('/post_category/edit/{id}', [
        'uses' => 'PostCategoriesController@edit', 
'as' => 'post_category.edit'
]); 

Route::get('/post_category/delete/{id}', [
        'uses' => 'PostCategoriesController@delete', 
'as' => 'post_category.delete'
]); 

Route::post('/post_category/update/{id}', [
        
            'uses' => 'PostCategoriesController@update', 
'as' => 'post_category.update'
]); 

//Users & Profile
Route::get('/users', [
        'uses' => 'UsersController@index', 
'as' => 'users'
]);
Route::get('/user/create', [
        'uses' => 'UsersController@create', 
'as' => 'user.create'
]);

Route::post('/user/store', [
        'uses' => 'UsersController@store', 
'as' => 'user.store'
]);

Route::get('/user/edit/{id}', [
        'uses' => 'UsersController@edit', 
'as' => 'user.edit'
]);

Route::get('/user/delete/{id}', [
        'uses' => 'UsersController@delete', 
'as' => 'user.delete'
]);

Route::get('user/profile',[
        'uses'=> 'ProfilesController@index',
        'as' => 'user.profile'
]);
Route::post('user/profile/update',[
        'uses'=> 'ProfilesController@update',
        'as' => 'user.profile.update'
]);

//Attach this route to Admin middleware
Route::get('/user/admin/{id}', [
        'uses' => 'UsersController@admin',
        'as' => 'user.admin'
])->middleware('admin');

Route::get('/user/non-admin/{id}', [
        'uses' => 'UsersController@non_admin',
        'as' => 'user.non_admin'
])->middleware('admin');

//Tags

Route::get('/tags',[
        'uses' => 'TagsController@index',
        'as' => 'tags'
        ]);
        
        Route::get('/tag/read/{id}',[
                'uses' => 'TagsController@show',
                'as' => 'tag.show'
                ]);
        
        Route::get('/tag/create',[
                'uses' => 'TagsController@create',
                'as' => 'tag.create'
                ]);
        
        Route::post('/tag/store',[
                'uses' => 'TagsController@store',
                'as' => 'tag.store'
                ]);
        
        Route::get('/tag/edit/{id}',[
                'uses' => 'TagsController@edit',
                'as' => 'tag.edit'
                ]);
        Route::post('/tag/update/{id}',[
                'uses' => 'TagsController@update',
                'as' => 'tag.update'
                ]);
        
        Route::get('/tag/destroy/{id}',[
                'uses'=>'TagsController@destroy',
                'as' =>'tag.destroy'
        ]);

        //Settings
        Route::get('/settings',[
                'uses'=>'SettingsController@index',
                'as' =>'settings.index'
        ]);
        Route::post('/settings/update',[
               'uses'=>'SettingsController@update',
                'as' =>'settings.update' 
        ]);

        Route::get('/message',function(){
                return Message::display();
        });

        Route::resource('portfolios','PortfoliosController');
}); 

